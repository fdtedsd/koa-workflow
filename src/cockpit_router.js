const Router = require("koa-router");
const bodyParser = require("koa-bodyparser");
const { captureActorData, validateUUID } = require("./middlewares");
const cors = require('koa2-cors');
const cockpit_controller = require("./controllers/cockpit");
const cockpit_validator = require("./validators/cockpit");

module.exports = (opts = {}) => {
     const router = new Router();

     router.use(bodyParser());

     for (let middleware of opts.middlewares) {
          router.use(middleware);
     }

     router.use(captureActorData);

     router.use(cors(opts.corsOptions));

     router.get("/workflows/stats", cockpit_controller.fetchWorkflowsWithProcessStatusCount);

     router.post("/processes/:id/state", validateUUID, cockpit_validator.validateSetProcessState, cockpit_controller.setProcessState);
     router.post("/processes/:id/state/run", validateUUID, cockpit_controller.runPendingProcess);

     return router;
};
